require([ 'jquery', 'jquery/ui'], function($){

	$(document).ready(function(){
	  $('.change-container-classname:checked').parent().addClass('yellowBackground');
	        
	        
		 $('.change-container-classname').click(function() {
	        $('.change-container-classname').parent().removeClass('yellowBackground');
	        $(this).parent().addClass('yellowBackground');
	    });
		
	}); 

	$(document).ready(function(){
		/* Move the product options to another div on the product page */
		$('.product-lower-primary-column').appendTo('.product-primary-column');
		$('.detailed').insertAfter('.product-secondary-column');

		/* Move the price field behind the quantity field on the product page */
		$('.field.qty').append( $('.price-box.price-configured_price') );
	});

});
  
   

