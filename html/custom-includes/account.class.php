<?php

    /**
    * Class Account
    * Everything concerning the user's account
    */
    class account
    {       
        const deviceMax = 3;
        const ipMax = 10;
        const sessionDuration = 1;
        const sessionMax = 5;

        var $databaseClass;
        var $errors;
        var $succes;

        /**
        * @var object The database connection
        */
        private $db_connection = null;

        /**
        * the function "__construct()" automatically starts whenever an object of this class is created,
        * you know, when you do "$login = new Login();"
        */
        public function __construct($databaseClass)
        {
            if($databaseClass != "")
                $this->databaseClass = $databaseClass;
            $this->databaseClass = new database();

            $this->databasemClass = new databasem();

            // User tries to log out
            if (isset($_GET["logout"])) {
                $this->doLogout();
            }
            // User tries to register or log in via email and password
            elseif (isset($_POST["form-name"])) {
                // Login
                if ($_POST["form-name"] == 'user-login'){
                    $email = null;
                    $password = null;
                    if (isset($_POST["email"])) {
                        $email = $_POST["email"];
                    }
                    if (isset($_POST["password"])) {
                        $password = $_POST["password"];
                    }
                    $this->loginByPassword($email, $password);
                }

                // Registration
                if ($_POST["form-name"] == 'user-reg'){
                    $name = null;
                    $email = null;
                    $password = null;
                    $passwordRepeat = null;

                    if (isset($_POST["name"])) {
                        $name = $_POST["name"];
                    }
                    if (isset($_POST["email"])) {
                        $email = $_POST["email"];
                    }
                    if (isset($_POST["password"])) {
                        $password = $_POST["password"];
                    }
                    if (isset($_POST["password_repeat"])) {
                        $passwordRepeat = $_POST["password_repeat"];
                    }
                    $this->registerUserByPassword($name, $email, $password, $passwordRepeat);
                }

                // Link order
                if ($_POST["form-name"] == 'link-order'){
                    $id = null;
                    $code = null;
                    if (isset($_POST["id"])) {
                        $id = $_POST["id"];
                    }
                    if (isset($_POST["code"])) {
                        $code = $_POST["code"];
                    }
                    $this->linkOrder($id, $code);
                }
            }
            // Social login
            elseif (isset($_GET["provider"])) {
                switch ($_GET["provider"]) {
                    case 'Facebook':
                        $this->loginBySocialApi('Facebook');
                        break;
                    case 'Instagram':
                        $this->loginBySocialApi('Instagram');
                        break;
                    case 'Google':
                        $this->loginBySocialApi('Google');
                        break;
                }
            }

        }

        /**
        * Log the user in by social API
        *
        * @access public
        * @param string
        *
        */
        private function loginBySocialApi($provider){
            // Create a hybrid auth class
            $config_file = '../custom-includes/hybridauth-2.6.0/hybridauth/config.php';
            $auth_file = 'hybridauth-2.6.0/hybridauth/Hybrid/Auth.php'; 
            require_once( $auth_file );
            $hybridauth = new Hybrid_Auth($config_file);
            
            try {
                $adapter = $hybridauth->authenticate($provider);
                $profile = $adapter->getUserProfile();

                // Get user info if account already exists
                $user = $this->getUserBySocialId($provider,$profile->identifier);
                if (!$user){
                    // User does not exist, create one
                    if ($this->registerUserBySocialLogin($provider, $profile->identifier, $profile->displayName, $profile->email)){
                        // Registration succesfull, get the user info
                        $user = $this->getUserBySocialId($provider,$profile->identifier);
                        $this->succes[] = "Account geregistreerd, je ben nu ingelogd!";
                    }
                    else{
                        $this->errors[] = "Registration failed."; 
                    }
                }

                if ($user){
                    // Set results of succesful login
                    $_SESSION['user_id'] = $user[0]["id"];
                    $_SESSION['user_username'] = $user[0]["username"];
                    $_SESSION['user_email'] = $user[0]["email"];
                    $_SESSION['user_name'] = $user[0]["name"];
                    $_SESSION['user_login_method'] = $provider;
                    $_SESSION['user_login_status'] = 1;
                }

            }
            catch (Exception $e){
                switch( $e->getCode() )
                {
                    case 0 : $this->errors[] = "Unspecified error."; break;
                    case 1 : $this->errors[] = "Hybridauth configuration error."; break;
                    case 2 : $this->errors[] = "Provider not properly configured."; break;
                    case 3 : $this->errors[] = "Unknown or disabled provider."; break;
                    case 4 : $this->errors[] = "Missing provider application credentials."; break;
                    case 5 : $this->errors[] = "Authentication failed The user has canceled the authentication or the provider refused the connection."; break;
                    case 6 : $this->errors[] = "User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.";
                        $adapter->logout();
                        break;
                    case 7 : $this->errors[] = "User not connected to the provider.";
                        $adapter->logout();
                        break;
                    case 8 : $this->errors[] = "Provider does not support this feature."; break;
                }
            }

        }

        /**
        * Log the user in by username & password
        *
        * @access public
        * @param string
        * @param string
        *
        */
        private function loginByPassword($username, $password)
        {
            // check login form contents
            if (empty($username)) {
                $this->errors[] = "Email adres is leeg";
            } elseif (empty($password)) {
                $this->errors[] = "Wachtwoord is leeg";
            } elseif (!empty($username) && !empty($password)) {
                if ($username && $password) {
                    $username = $this->databaseClass->filter($username);
                    $password = $this->databaseClass->filter($password);

                    // Get user info if account exists
                    $user = $this->getUserByUsername($username);
                    // User exists
                    if ($user){
                        // using PHP 5.5's password_verify() function to check if the provided password fits
                        // the hash of that user's password
                        if (password_verify($password, $user[0]["password_hash"])) {

                            // Set results of succesful login
                            $_SESSION['user_id'] = $user[0]["id"];
                            $_SESSION['user_username'] = $user[0]["username"];
                            $_SESSION['user_email'] = $user[0]["email"];
                            $_SESSION['user_name'] = $user[0]["name"];
                            $_SESSION['user_login_method'] = 'password';
                            $_SESSION['user_login_status'] = 1;

                        } else {
                            $this->errors[] = "Verkeerde email/ wachtwoord combinatie";
                        }
                    }
                    // User does not exist
                    else{
                        $this->errors[] = "Verkeerde email/ wachtwoord combinatie";
                    }
                }
            }
        }

        /**
        * perform the logout
        */
        public function doLogout()
        {
            // delete the session of the user
            $_SESSION = array();
            session_destroy();
        }

        /**
        * simply return the current state of the user's login
        * @return boolean user's login status
        */
        public function isUserLoggedIn()
        {
            if (isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] == 1) {
                return 'ok';
            }
        }

        /**
        * Retrieve the user by social identifier
        *
        * @access public
        * @param string
        * @param string
        * @return array
        *
        */
        public function getUserBySocialId($provider, $id){
            if ($provider && $id) {
                $provider = strtolower($this->databaseClass->filter($provider));
                $id = $this->databaseClass->filter($id);
                $query = "SELECT
                U.*
                FROM 
                `Users` U
                WHERE U.".$provider."_identifier = '".$id."';"; 
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Retrieve the user by username
        *
        * @access public
        * @param string
        * @return array
        *
        */
        public function getUserByUsername($username){
            if ($username) {
                $username = $this->databaseClass->filter($username);
                $query = "SELECT
                U.*
                FROM 
                `Users` U
                WHERE U.`username` = '".$username."';"; 
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Register user by social login
        *
        * @access public
        * @param string
        * @param string
        * @param string
        * @param string
        * @return array
        *
        */
        private function registerUserBySocialLogin($provider, $identifier, $name, $email){
            if ($provider && $identifier) {
                $provider = strtolower($this->databaseClass->filter($provider));
                $identifier = $this->databaseClass->filter($identifier);
                $name = $this->databaseClass->filter($name);
                $email = $this->databaseClass->filter($email);
                $data = array(
                    "`email`"                           => $email,
                    "`name`"                            => $name,
                    "`".$provider."_identifier`"        => $identifier,
                );
                $result = $this->databaseClass->insert('Users', $data);
                return $result;
            }
        }

        /**
        * Register user by username and password
        *
        * @access public
        * @param string
        * @param string
        * @param string
        * @param string
        *
        */
        private function registerUserByPassword($name, $email, $password, $passwordRepeat){
            if (empty($email)) {
                $this->errors[] = "Geen email adres";
            } elseif (empty($password) || empty($passwordRepeat)) {
                $this->errors[] = "Leeg wachtwoord";
            } elseif ($password !== $passwordRepeat) {
                $this->errors[] = "Wachtwoorden zijn niet gelijk";
            } elseif (strlen($password) < 6) {
                $this->errors[] = "Het gekozen wachtwoord is niet 6 karakters of langer";
            } elseif (strlen($email) > 100 || strlen($email) < 2) {
                $this->errors[] = "Email adres kan niet korter zijn dan 2 or langer dan 100 karakters";
            } elseif (empty($name)) {
                $this->errors[] = "Naam is leeg";
            } elseif (strlen($name) > 128) {
                $this->errors[] = "Naam kan niet langer zijn dan 128 karakters";
            } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->errors[] = "Het email adres is geen valide email adres";
            } elseif (!empty($email)
                && strlen($email) <= 100
                && strlen($email) >= 2
                && !empty($name)
                && strlen($name) <= 128
                && filter_var($email, FILTER_VALIDATE_EMAIL)
                && !empty($password)
                && !empty($passwordRepeat)
                && ($password === $passwordRepeat)
            ){
                if ($name && $email && $password && $passwordRepeat) {
                    $name = $this->databaseClass->filter($name);
                    $email = $this->databaseClass->filter($email);
                    $password = $this->databaseClass->filter($password);
                    $passwordRepeat = $this->databaseClass->filter($passwordRepeat);

                    // crypt the user's password with PHP 5.5's password_hash() function, results in a 60 character
                    // hash string. the PASSWORD_DEFAULT constant is defined by the PHP 5.5, or if you are using
                    // PHP 5.3/5.4, by the password hashing compatibility library
                    $passwordHashed = password_hash($password, PASSWORD_DEFAULT);

                    // Get user info if account already exists
                    $user = $this->getUserByUsername($email);
                    // User already exists
                    if ($user){
                        $this->errors[] = "Dit email adres is al in gebruik";
                    }
                    // User does not exist yet
                    else {
                        // Create account
                        $data = array(
                            "`username`"                        => $email,
                            "`password_hash`"                   => $passwordHashed,
                            "`email`"                           => $email,
                            "`name`"                            => $name,
                        );
                        $result = $this->databaseClass->insert('Users', $data);
                        if($result) {
                            $this->loginByPassword($email, $password);
                            $this->succes[] = "Account geregistreerd, je ben nu ingelogd!";
                        }
                        else {
                            $this->errors[] = "Registratie mislukt";
                        }
                    }
                }
            }
        }

        /**
        * Retrieve the device log
        *
        * @access public
        * @param integer
        * @param string
        * @return array
        *
        */
        public function getDeviceLog($userId, $deviceId){
            if($userId && $deviceId){
                $userId = $this->databaseClass->filter($userId);
                $deviceId = $this->databaseClass->filter($deviceId);
                $query = "SELECT
                UD.*
                FROM 
                UserDevices UD
                WHERE UD.`user`=".$userId." AND UD.`device`='".$deviceId."';";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Retrieve the device logs
        *
        * @access public
        * @param integer
        * @return array
        *
        */
        public function getDeviceLogs($userId){
            if($userId){
                $userId = $this->databaseClass->filter($userId);
                $query = "SELECT
                UD.*
                FROM 
                UserDevices UD
                WHERE UD.`user`=".$userId.";";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Retrieve the ip log
        *
        * @access public
        * @param integer
        * @param string
        * @return array
        *
        */
        public function getIpLog($userId, $ip){
            if($userId && $ip){
                $userId = $this->databaseClass->filter($userId);
                $ip = $this->databaseClass->filter($ip);
                $query = "SELECT
                UIA.*
                FROM 
                UserIpAddresses UIA
                WHERE UIA.`user`=".$userId." AND UIA.`ip`='".$ip."';";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Retrieve the ip logs
        *
        * @access public
        * @param integer
        * @return array
        *
        */
        public function getIpLogs($userId){
            if($userId){
                $userId = $this->databaseClass->filter($userId);
                $query = "SELECT
                UIA.*
                FROM 
                UserIpAddresses UIA
                WHERE UIA.`user`=".$userId.";";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Log the user action
        *
        * @access public
        * @param integer
        * @param string
        * @param string
        * @return integer
        *
        */
        public function logUserAction($userId, $deviceId, $ip){
            if ($userId && $deviceId && $ip) {
                $userId = $this->databaseClass->filter($userId);
                $deviceId = $this->databaseClass->filter($deviceId);
                $ip = $this->databaseClass->filter($ip);
                $ts = date("Y-m-d H:i:s");

                // Check if a device log exists
                if ($deviceLog = $this->getDeviceLog($userId, $deviceId)){                   
                    // Device already exists, update the last activity and the activity count
                    $id = $deviceLog[0]["id"];
                    $activityCount = $deviceLog[0]["activity_count"] + 1;
                    $data = array(
                        "last_activity"       => $ts, 
                        "activity_count"      => $activityCount
                    );
                    $this->databaseClass->update('UserDevices', $data, array("id" => $id), 1);
                }
                else {
                    // Device does not exist, insert the new device
                    if ($deviceLogs = $this->getDeviceLogs($userId)){
                        // User already logged in on a different device before,
                        $deviceCount = 0;
                        foreach ($deviceLogs as $x){
                            $deviceCount++;
                        }

                        $authorized = 0;
                        if ($deviceCount < self::deviceMax){
                            // Device authorized
                            $authorized = 1;
                        }
                    }
                    else {
                        // User never logged in before, device authorized
                        $authorized = 1;
                    }

                    // Insert the new device
                    $data = array(
                        "`user`"                => $userId,
                        "`device`"              => $deviceId,
                        "`last_activity`"       => $ts,
                        "`activity_count`"      => 1,
                        "`authorized`"          => $authorized,
                    );
                    $this->databaseClass->insert('UserDevices', $data);
                }

                // Check if a ip log exists
                if ($ipLog = $this->getIpLog($userId, $ip)){                   
                    // IP already exists, update the last activity and the activity count
                    $id = $ipLog[0]["id"];
                    $activityCount = $ipLog[0]["activity_count"] + 1;
                    $data = array(
                        "last_activity"       => $ts, 
                        "activity_count"      => $activityCount
                    );
                    $this->databaseClass->update('UserIpAddresses', $data, array("id" => $id), 1);
                }
                else {
                    // IP does not exist, insert the new IP
                    if ($ipLogs = $this->getIpLogs($userId)){
                        // User already logged in on a different ip before,
                        $ipCount = 0;
                        foreach ($ipLogs as $x){
                            $ipCount++;
                        }

                        $authorized = 0;
                        if ($ipCount < self::ipMax){
                            // Device authorized
                            $authorized = 1;
                        }
                    }
                    else {
                        // User never logged in before, ip authorized
                        $authorized = 1;
                    }

                    // Insert the new ip
                    $data = array(
                        "`user`"                => $userId,
                        "`ip`"                  => $ip,
                        "`last_activity`"       => $ts,
                        "`activity_count`"      => 1,
                        "`authorized`"          => $authorized,
                    );
                    $this->databaseClass->insert('UserIpAddresses', $data);
                }

                $_SESSION['device'] = $deviceId;
                $_SESSION['ip'] = $ip;

                return true;
            }
        }

        /**
        * Get authorization
        *
        * @access public
        * @return string
        *
        */
        public function isUserAuthorized(){
            if (isset($_SESSION['user_id']) && isset($_SESSION['device']) && isset($_SESSION['ip'])) {
                $userId = $this->databaseClass->filter($_SESSION['user_id']);
                $deviceId = $this->databaseClass->filter($_SESSION['device']);
                $ip = $this->databaseClass->filter($_SESSION['ip']);

                $deviceLog = $this->getDeviceLog($userId, $deviceId);
                $ipLog = $this->getIpLog($userId, $ip);
                $ipLogs = $this->getIpLogs($userId);

                if ($deviceLog[0]["authorized"] <> 1){
                    // Device not authorized
                    return 'Dit apparaat is niet geautoriseerd om samenvattingen mee te bekijken.';
                }
                else {
                    if ($ipLog[0]["authorized"] <> 1){
                        // IP not authorized
                        return 'Dit adres is niet geautoriseerd om samenvattingen mee te bekijken.';
                    }
                    else {
                        $sessionCount = 0;
                        foreach ($ipLogs as $logEntry){
                            $hourdiff = round((strtotime(date("Y-m-d H:i:s")) - strtotime($logEntry["last_activity"]))/3600, 1);
                            if ($hourdiff <= self::sessionDuration){
                                $sessionCount++;
                            }
                        }

                        if ($sessionCount > self::sessionMax){
                            // Too many sessions
                            return 'Er zijn teveel sessies op dit account het afgelopen uur, probeer het later nog een keer.';
                        }
                        else {
                            // Authorized
                            return 'ok';
                        }

                    }
                }
            }
            else {
                return 'De autorisatie is nog niet voltooid, probeer het over een paar seconden nog een keer.';
            }
        }

        /**
        * Get authorization for product
        *
        * @access public
        * @param integer
        * @return string
        *
        */
        public function isProductViewAuthorized($productId){
            if ($productId && isset($_SESSION['user_id'])) {
                $productId = $this->databaseClass->filter($productId);
                $userId = $this->databaseClass->filter($_SESSION['user_id']);

                // store v2
                $query = "SELECT
                EAC.`order_id` AS 'order_id',
                EAC.`sku` AS 'sku',
                P.`id` AS 'product_id',
                P.`name` AS 'product_name',
                P.`format` AS 'product_format'
                FROM `ExOvAccessCodes` EAC
                INNER JOIN `Products` P ON EAC.`sku` = P.`sku`
                WHERE EAC.`user_id` = ".$userId." AND P.`id` = ".$productId." AND P.`format` IN ('digital', 'hardcopy + digital');";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return 'ok';
                }
                else {
                    // Check store v1
                    $query = "SELECT
                    O.id AS 'order_id',
                    OL.id AS 'orderline_id',
                    P.id AS 'product_id'
                    FROM
                    `Orders` O
                    INNER JOIN `OrderLines` OL ON O.id = OL.`order` 
                    INNER JOIN `Products` P ON P.id = OL.`product`
                    WHERE O.`user_id` = ".$userId." AND P.`id` = ".$productId." AND OL.`delivery_status` IN ('paid','packed','shipped','promo') AND P.`format` IN ('digital', 'hardcopy + digital');";
                    if ($results = $this->databaseClass->get_results( $query )) {
                        return 'ok';
                    }
                }
            }
        }

        /**
        * Get the user's linked products (store v2)
        *
        * @access public
        * @param integer
        * @return array
        *
        */
        public function getLinkedProducts($userId){
            if ($userId) {
                $userId = $this->databaseClass->filter($userId);

                $query = "SELECT
                EAC.`order_id` AS 'order_id',
                EAC.`sku` AS 'sku',
                P.`id` AS 'product_id',
                P.`name` AS 'product_name',
                P.`format` AS 'product_format'
                FROM `ExOvAccessCodes` EAC
                INNER JOIN `Products` P ON EAC.`sku` = P.`sku`
                WHERE EAC.`user_id` = ".$userId."
                ORDER BY EAC.`order_id` ASC;";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Retrieve the user's linked orders (store v1)
        *
        * @access public
        * @param integer
        * @return array
        *
        */
        public function getLinkedOrders($userId){
            if($userId){
                $userId = $this->databaseClass->filter($userId);
                $query = "SELECT
                O.*
                FROM 
                `Orders` O
                WHERE O.`user_id`=".$userId.";";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Link an order to a user
        *
        * @access public
        * @param integer
        * @param string
        *
        */
        public function linkOrder($orderId, $orderCode){
            $userId = $_SESSION['user_id'];
            if($userId && $orderId && $orderCode){
                $orderId = $this->databaseClass->filter($orderId);
                $orderCode = $this->databaseClass->filter($orderCode);

                $order = new order($this->databaseClass);
                $orderInfo = $order->getOrderInformation($orderId);
                if ($orderInfo){
                    // Order found (store v1 & v2)
                    if ($orderInfo[0]["exov_store"] == 'v1') {
                        // Store v1
                        if (!$orderInfo[0]["user_id"]) {
                            // Order not yet linked
                            if (substr($orderInfo[0]["id_hash"],0,16) == $orderCode){
                                // Code is correct
                                if ($orderInfo[0]["payment_status"] == 'paid' || $orderInfo[0]["payment_status"] == 'packed' ||$orderInfo[0]["payment_status"] == 'shipped' ||$orderInfo[0]["payment_status"] == 'promo'){
                                    // Order is paid
                                    $this->databaseClass->update("Orders",array("user_id" => $userId), array("id" => $orderId), 1);
                                    $this->succes[] = "Bestelling succesvol gekoppeld. Je kunt aan de slag met leren!";
                                }
                                else {
                                    // Order is not paid
                                    $this->errors[] = "De bestelling is nog niet betaald.";
                                }
                            }
                            else {
                                // Code is incorrect
                                $this->errors[] = "Vul een geldig bestelnummer plus link code in. Deze kun je vinden in de bevestigingsmail na je bestelling.";
                            }
                        }
                        else {
                            // Order already linked
                            if ($orderInfo[0]["user_id"] == $userId){
                                // Order already linked to current user
                                $this->errors[] = "De bestelling is reeds aan je account gekoppeld.";
                            }
                            else {
                                // Order linked to another user
                                $this->errors[] = "De bestelling is reeds aan een ander account gekoppeld.";
                            }
                        }
                    }
                    elseif ($orderInfo[0]["exov_store"] == 'v2') {
                        // Store v2
                        $linkedAccount = $this->getLinkedAccount($orderId,$orderCode);
                        if (!$linkedAccount) {
                            // Order not yet linked
                            $sku = $this->getSkuFromAccessCode($orderId,$orderCode);
                            if ($sku) {
                                if ($orderInfo[0]["grand_total"] > 0 && ($orderInfo[0]["grand_total"] == $orderInfo[0]["total_paid"])) {
                                    // Order is paid
                                    $data = array(
                                        "`order_id`"            => $orderId,
                                        "`access_code`"         => $orderCode,
                                        "`user_id`"             => $userId,
                                        "`sku`"                 => $sku,
                                    );
                                    $this->databaseClass->insert('ExOvAccessCodes', $data);
                                    $this->succes[] = "Bestelling succesvol gekoppeld. Je kunt aan de slag met leren!";
                                }
                                else {
                                    // Order is not paid
                                    $this->errors[] = "De bestelling is nog niet betaald.";
                                }
                            }
                            else {
                                // Code is incorrect
                                $this->errors[] = "Vul een geldig bestelnummer plus link code in. Deze kun je vinden in de bevestigingsmail na je bestelling.";
                            }
                        }
                        else {
                            // Order already linked
                            if ($linkedAccount == $userId){
                                // Order already linked to current user
                                $this->errors[] = "De bestelling is reeds aan je account gekoppeld.";
                            }
                            else {
                                // Order linked to another user
                                $this->errors[] = "De bestelling is reeds aan een ander account gekoppeld.";
                            }
                        }
                    }
                }
                else {
                    // Order not found
                    $this->errors[] = "De opgegeven bestelling is niet gevonden.";
                }
            }
            else {
                // Info not complete
                $this->errors[] = "Het koppelen van de bestelling is mislukt, probeer het nog een keer.";
            }
        }

        /**
        * Get linked account for an order and access code combination
        *
        * @access public
        * @param integer
        * @param string
        * @return integer
        *
        */
        public function getLinkedAccount($orderId,$accessCode){
            if ($orderId && $accessCode) {
                $orderId = $this->databaseClass->filter($orderId);
                $accessCode = $this->databaseClass->filter($accessCode);

                $query = "SELECT
                AC.user_id AS 'user_id'
                FROM
                `ExOvAccessCodes` AC
                WHERE AC.`order_id` = ".$orderId." AND AC.`access_code` = '".$accessCode."';";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results[0]["user_id"];
                }
            }
        }


        /**
        * Get the sku from the entered access code
        *
        * @access public
        * @param integer
        * @param string
        * @return string
        *
        */
        public function getSkuFromAccessCode($orderId,$accessCode){
            if ($orderId && $accessCode) {
                $orderId = $this->databaseClass->filter($orderId);
                $accessCode = $this->databaseClass->filter($accessCode);

                $query = "SELECT
                A.order_id AS 'order_id',
                A.accesscode AS 'access_code'
                FROM
                `expert_multiple_accesscode` A
                WHERE A.`order_id` = ".$orderId.";";
                if ($results = $this->databasemClass->get_results( $query )) {
                    $json = $results[0]["access_code"];
                    $array = json_decode($json, true);

                    foreach($array as $sku){
                        $skuDb = key($sku);
                        foreach ($sku[$skuDb] as $code){
                            $idDb = key($code);
                            $accessCodeDb = $code[$idDb];
                            if($accessCode == $accessCodeDb){
                                // Access code found
                                return $skuDb;
                            }
                        }
                    }
                }
            }
        }


    }
