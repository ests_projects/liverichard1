<?php

    /**
    * HybridAuth
    * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
    * (c) 2009-2015, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
    */
    // ----------------------------------------------------------------------------------------
    //	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
    // ----------------------------------------------------------------------------------------

    return
    array(
        "base_url" => Setting::baseUrlHttps."/custom-includes/hybridauth-2.6.0/hybridauth/",
        "providers" => array(
            // openid providers
            "OpenID" => array(
                "enabled" => false
            ),
            "Yahoo" => array(
                "enabled" => false,
                "keys" => array("key" => "", "secret" => ""),
            ),
            "AOL" => array(
                "enabled" => false
            ),
            "Google" => array(
                "enabled" => true,
                "keys" => array("id" => Setting::socialLoginKeyGoogle, "secret" => Setting::socialLoginSecretGoogle),
                "scope" => "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"
            ),
            "Facebook" => array(
                "enabled" => true,
                "keys" => array("id" => Setting::socialLoginKeyFacebook, "secret" => Setting::socialLoginSecretFacebook),
                "trustForwarded" => false,
                "scope" => "email"
            ),
            "Instagram" => array(
                "enabled" => true,
                "keys" => array("id" => Setting::socialLoginKeyInstagram, "secret" => Setting::socialLoginSecretInstagram),
            ),
            "Twitter" => array(
                "enabled" => false,
                // Test
                "keys" => array("key" => "AfikV1F0y1ka8Yu2xd9Yo2RYA ", "secret" => "98BhicRxbwFNNnOXSlwrAqP2aiLCag9uv1RfTj4TPbLUjsklCD"),
                // Live
                //"keys" => array("key" => "adP5by2G22SPcowVxZAVBNx5h", "secret" => "RJNIwaFlVyfPpU2kzk5y69B5GyFJzfjvsM3tGDGFG7gQIkqq1C"),
                "includeEmail" => false
            ),
            // windows live
            "Live" => array(
                "enabled" => false,
                "keys" => array("id" => "", "secret" => "")
            ),
            "LinkedIn" => array(
                "enabled" => false,
                "keys" => array("key" => "", "secret" => "")
            ),
            "Foursquare" => array(
                "enabled" => false,
                "keys" => array("id" => "", "secret" => "")
            ),
        ),
        // If you want to enable logging, set 'debug_mode' to true.
        // You can also set it to
        // - "error" To log only error messages. Useful in production
        // - "info" To log info and error messages (ignore debug messages)
        "debug_mode" => false,
        // Path to file writable by the web server. Required if 'debug_mode' is not false
        "debug_file" => "",
    );
